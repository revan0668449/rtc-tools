Delft-FEWS I/O
==============

.. autoclass:: rtctools.optimization.pi_mixin.PIMixin
    :members: min_timeseries_id, max_timeseries_id
    :show-inheritance: